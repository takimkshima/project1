using UnityEngine;

namespace Spaceship
{
    public abstract class Basespaceship : MonoBehaviour
    {
        [SerializeField] protected Bullet defaultBullet;
        [SerializeField] protected Bullet butterflyBullet;
        [SerializeField] protected Transform gunPosition;
        
        public int Hp { get; protected set; }
        public float Speed { get; private set; }
        public Bullet Bullet { get; private set; }

        public Bullet NewBullet { get; private set; }

        protected void Init(int hp, float speed, Bullet bullet , Bullet newbullet)
        {
            Hp = hp;
            Speed = speed;
            Bullet = bullet;
            NewBullet = newbullet;
        }

        public virtual void newBullet()
        {

        }
        public virtual void Fire()
        {

        }
    }
}