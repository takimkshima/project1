using System;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private double fireRate = 1;
        private float fireCounter = 0;
        [SerializeField] private AudioClip enemyFireSound;
        [SerializeField] private float enemyFireSoundVolume = 0.3f;
       
        [SerializeField] private AudioClip enemyExplodeSound;
        [SerializeField] private float enemyExplodeSoundVolume = 0.3f;

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet, butterflyBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
            AudioSource.PlayClipAtPoint(enemyExplodeSound, Camera.main.transform.position, enemyFireSoundVolume);
        }

        public override void Fire()
        {
            // TODO: Implement this later
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                AudioSource.PlayClipAtPoint(enemyFireSound, Camera.main.transform.position, enemyFireSoundVolume);
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }
        }
    }
}